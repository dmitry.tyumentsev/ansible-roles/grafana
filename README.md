Grafana role
=========

Installs Grafana on Debian/Ubuntu servers.

The Grafana repository is not available for users from Russia.

Use yandex mirror repository
```yml
grafana_apt_repo_url: https://mirror.yandex.ru/mirrors/packages.grafana.com/oss/deb
grafana_apt_gpg_key_url: https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xb53ae77badb630a683046005963fa27710458545
```

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.grafana
```
